namespace TreinamentoAPIMantisBase2.Objects
{
    public class User
    {
        private string username;
        private string password;
        private string realName;
        private string email;
        private string accessLevel;
        private string enabled;
        private string protectedUser;

        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string RealName { get => realName; set => realName = value; }
        public string Email { get => email; set => email = value; }
        public string AccessLevel { get => accessLevel; set => accessLevel = value; }
        public string Enabled { get => enabled; set => enabled = value; }
        public string ProtectedUser { get => protectedUser; set => enabled = value; }
    }
}