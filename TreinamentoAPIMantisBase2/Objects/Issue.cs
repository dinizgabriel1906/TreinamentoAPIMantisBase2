namespace TreinamentoAPIMantisBase2.Objects
{
    public class Issue
    {
        private string summary;
        private string description;
        private string additionalInformation;
        private string projectId;
        private string projectName;
        private string categoryId;
        private string categoryName;
        private string handlerName;
        private string viewStateId;
        private string viewStateName;
        private string priorityName;
        private string severityName;
        private string reproducibilityName;
        private string sticky;
        private string fieldId;
        private string fieldName;
        private string fieldValue;
        private string tags; // Change to array

        public string Summary { get => summary; set => summary = value; }
        public string Description { get => description; set => description = value; }
        public string AdditionalInformation { get => additionalInformation; set => additionalInformation = value; }
        public string ProjectId { get => projectId; set => projectId = value; }
        public string ProjectName { get => projectName; set => projectName = value; }
        public string CategoryId { get => categoryId; set => categoryId = value; }
        public string CategoryName { get => categoryName; set => categoryName = value; }
        public string HandlerName { get => handlerName; set => handlerName = value; }
        public string ViewStateId { get => viewStateId; set => viewStateId = value; }
        public string ViewStateName { get => viewStateName; set => viewStateName = value; }
        public string PriorityName { get => priorityName; set => priorityName = value; }
        public string SeverityName { get => severityName; set => severityName = value; }
        public string ReproducibilityName { get => reproducibilityName; set => reproducibilityName = value; }
        public string Sticky { get => sticky; set => sticky = value; }
        public string FieldId { get => fieldId; set => fieldId = value; }
        public string FieldName { get => fieldName; set => fieldName = value; }
        public string FieldValue { get => fieldValue; set => fieldValue = value; }
        public string Tags { get => tags; set => tags = value; }

    }
}
