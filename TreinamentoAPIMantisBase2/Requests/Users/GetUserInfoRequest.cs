using RestSharp;
using TreinamentoAPIMantisBase2.Bases;
using TreinamentoAPIMantisBase2.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreinamentoAPIMantisBase2.Requests.Issues
{
    public class GetUserInfoRequest : RequestBase
    {
        public GetUserInfoRequest()
        {
            requestService = "/api/rest/users/me";
            method = Method.GET;
            headers.Add("Authorization", JsonBuilder.ReturnParameterAppSettings("TOKEN"));
        }
    }
}
