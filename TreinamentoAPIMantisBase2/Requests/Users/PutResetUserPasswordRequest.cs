using RestSharp;
using TreinamentoAPIMantisBase2.Bases;
using TreinamentoAPIMantisBase2.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreinamentoAPIMantisBase2.Requests.Issues
{
    public class PutResetUserPasswordRequest : RequestBase
    {
        public PutResetUserPasswordRequest(string userId)
        {
            requestService = "/api/rest/users/" + userId + "/reset";
            method = Method.PUT;
            headers.Add("Authorization", JsonBuilder.ReturnParameterAppSettings("TOKEN"));
        }
    }
}
