using RestSharp;
using TreinamentoAPIMantisBase2.Bases;
using TreinamentoAPIMantisBase2.Objects;
using System.IO;
using TreinamentoAPIMantisBase2.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreinamentoAPIMantisBase2.Requests.Issues
{
    public class PostCreateUserRequest : RequestBase
    {
        public PostCreateUserRequest()
        {
            requestService = "/api/rest/users";
            method = Method.POST;
            headers.Add("Authorization", JsonBuilder.ReturnParameterAppSettings("TOKEN"));
        }

        public void setJsonBody(User user) 
        {
            jsonBody = File.ReadAllText(GeneralHelpers.ReturnProjectPath() + "Jsons/UserJson.json", Encoding.UTF8);
            jsonBody = jsonBody.Replace("$username", user.Username);
            jsonBody = jsonBody.Replace("$password", user.Password);
            jsonBody = jsonBody.Replace("$realname", user.RealName);
            jsonBody = jsonBody.Replace("$email", user.Email);
            jsonBody = jsonBody.Replace("$level", user.AccessLevel);
            jsonBody = jsonBody.Replace("$enabled", user.Enabled);
            jsonBody = jsonBody.Replace("$protected", user.ProtectedUser);
        }
    }
}
