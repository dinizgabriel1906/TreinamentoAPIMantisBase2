using RestSharp;
using TreinamentoAPIMantisBase2.Bases;
using TreinamentoAPIMantisBase2.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreinamentoAPIMantisBase2.Requests.Issues
{
    public class GetFilterByIdRequest : RequestBase
    {
        public GetFilterByIdRequest(string filterId)
        {
            requestService = "/api/rest/filters/" + filterId;
            method = Method.GET;
            headers.Add("Authorization", JsonBuilder.ReturnParameterAppSettings("TOKEN"));
        }
    }
}
