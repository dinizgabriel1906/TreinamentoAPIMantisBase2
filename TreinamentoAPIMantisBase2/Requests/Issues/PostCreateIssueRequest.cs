using RestSharp;
using TreinamentoAPIMantisBase2.Bases;
using TreinamentoAPIMantisBase2.Objects;
using TreinamentoAPIMantisBase2.Helpers;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace TreinamentoAPIMantisBase2.Requests.Issues
{
    public class PostCreateIssueRequest : RequestBase
    {
        public PostCreateIssueRequest(string pageSize, string page)
        {
            requestService = "/api/rest/issues";
            method = Method.POST;
            headers.Add("Authorization", JsonBuilder.ReturnParameterAppSettings("TOKEN"));
        }

        public void setJsonBody(Issue issue) 
        {
            jsonBody = File.ReadAllText(GeneralHelpers.ReturnProjectPath() + "Jsons/IssueMinimalJson.json", Encoding.UTF8);
            jsonBody = jsonBody.Replace("$summary", issue.Summary);
            jsonBody = jsonBody.Replace("$description", issue.Description);
            jsonBody = jsonBody.Replace("$additionalInformation", issue.AdditionalInformation);
            jsonBody = jsonBody.Replace("$projectId", issue.ProjectId);
            jsonBody = jsonBody.Replace("$projectName", issue.ProjectName);
            jsonBody = jsonBody.Replace("$categoryId", issue.CategoryId);
            jsonBody = jsonBody.Replace("$categoryName", issue.CategoryName);
            jsonBody = jsonBody.Replace("$handlerName", issue.HandlerName);
            jsonBody = jsonBody.Replace("$viewStateId", issue.ViewStateId);
            jsonBody = jsonBody.Replace("$viewStateName", issue.ViewStateName);
            jsonBody = jsonBody.Replace("$priorityName", issue.PriorityName);
            jsonBody = jsonBody.Replace("$severityName", issue.SeverityName);
            jsonBody = jsonBody.Replace("$reproducibilityName", issue.ReproducibilityName);
            jsonBody = jsonBody.Replace("$sticky", issue.Sticky);
            jsonBody = jsonBody.Replace("$fieldId", issue.FieldId);
            jsonBody = jsonBody.Replace("$fieldName", issue.FieldName);
            jsonBody = jsonBody.Replace("$fieldValue", issue.FieldValue);
            jsonBody = jsonBody.Replace("$tagName", issue.Tags);
        }
    }
}
