using RestSharp;
using TreinamentoAPIMantisBase2.Bases;
using TreinamentoAPIMantisBase2.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreinamentoAPIMantisBase2.Requests.Issues
{
    public class GetIssueForProjectRequest : RequestBase
    {
        public GetIssueForProjectRequest(string projectId)
        {
            requestService = "/api/rest/issues";
            method = Method.GET;
            parameters.Add("project_id", projectId);
            headers.Add("Authorization", JsonBuilder.ReturnParameterAppSettings("TOKEN"));
        }
    }
}
