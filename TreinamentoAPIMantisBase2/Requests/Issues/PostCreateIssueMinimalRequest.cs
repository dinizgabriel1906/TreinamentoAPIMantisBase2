using RestSharp;
using TreinamentoAPIMantisBase2.Bases;
using TreinamentoAPIMantisBase2.Objects;
using TreinamentoAPIMantisBase2.Helpers;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace TreinamentoAPIMantisBase2.Requests.Issues
{
    public class PostCreateIssueMinimalRequest : RequestBase
    {
        public PostCreateIssueMinimalRequest(string pageSize, string page)
        {
            requestService = "/api/rest/issues";
            method = Method.POST;
            headers.Add("Authorization", JsonBuilder.ReturnParameterAppSettings("TOKEN"));
        }

        public void setJsonBody(Issue issue) 
        {
            jsonBody = File.ReadAllText(GeneralHelpers.ReturnProjectPath() + "Jsons/IssueMinimalJson.json", Encoding.UTF8);
            jsonBody = jsonBody.Replace("$summary", issue.Summary);
            jsonBody = jsonBody.Replace("$description", issue.Description);
            jsonBody = jsonBody.Replace("$categoryName", issue.CategoryName);
            jsonBody = jsonBody.Replace("$projectName", issue.ProjectName);
        }
    }
}
