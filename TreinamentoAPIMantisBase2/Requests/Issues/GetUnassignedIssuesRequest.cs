using RestSharp;
using TreinamentoAPIMantisBase2.Bases;
using TreinamentoAPIMantisBase2.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreinamentoAPIMantisBase2.Requests.Issues
{
    public class GetUnassignedIssuesRequest : RequestBase
    {
        public GetUnassignedIssuesRequest()
        {
            requestService = "/api/rest/issues";
            method = Method.GET;
            parameters.Add("filter_id", "unassigned");
            headers.Add("Authorization", JsonBuilder.ReturnParameterAppSettings("TOKEN"));
        }
    }
}
