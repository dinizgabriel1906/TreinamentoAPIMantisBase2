using RestSharp;
using TreinamentoAPIMantisBase2.Bases;
using TreinamentoAPIMantisBase2.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreinamentoAPIMantisBase2.Requests.Issues
{
    public class GetIssueByIdRequest : RequestBase
    {
        public GetIssueByIdRequest(string issueId)
        {
            requestService = "/api/rest/issues";
            method = Method.GET;
            parameters.Add("issue_id", issueId);
            headers.Add("Authorization", JsonBuilder.ReturnParameterAppSettings("TOKEN"));
        }
    }
}
