using RestSharp;
using TreinamentoAPIMantisBase2.Bases;
using TreinamentoAPIMantisBase2.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreinamentoAPIMantisBase2.Requests.Issues
{
    public class GetAllIssuesRequest : RequestBase
    {
        public GetAllIssuesRequest(string pageSize, string page)
        {
            requestService = "/api/rest/issues";
            method = Method.GET;
            parameters.Add("page_size", pageSize);
            parameters.Add("page", page);
            headers.Add("Authorization", JsonBuilder.ReturnParameterAppSettings("TOKEN"));
        }
    }
}
