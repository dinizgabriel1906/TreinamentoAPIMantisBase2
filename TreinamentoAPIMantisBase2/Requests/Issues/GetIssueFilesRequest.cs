using RestSharp;
using TreinamentoAPIMantisBase2.Bases;
using TreinamentoAPIMantisBase2.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreinamentoAPIMantisBase2.Requests.Issues
{
    public class GetIssueFilesRequest : RequestBase
    {
        public GetIssueFilesRequest(string issueId)
        {
            requestService = "/api/rest/issues/" + issueId + "/files";
            method = Method.GET;
            headers.Add("Authorization", JsonBuilder.ReturnParameterAppSettings("TOKEN"));
        }
    }
}
