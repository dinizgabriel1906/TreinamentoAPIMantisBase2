using RestSharp;
using TreinamentoAPIMantisBase2.Bases;
using TreinamentoAPIMantisBase2.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreinamentoAPIMantisBase2.Requests.Issues
{
    public class DelIssueRequest : RequestBase
    {
        public DelIssueRequest(string issueId)
        {
            requestService = "/api/rest/issues";
            method = Method.DELETE;
            parameters.Add("issue_id", issueId);
            headers.Add("Authorization", JsonBuilder.ReturnParameterAppSettings("TOKEN"));
        }
    }
}
