using RestSharp;
using TreinamentoAPIMantisBase2.Bases;
using TreinamentoAPIMantisBase2.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreinamentoAPIMantisBase2.Requests.Issues
{
    public class GetIssueFileRequest : RequestBase
    {
        public GetIssueFileRequest(string issueId, string fileId)
        {
            requestService = "/api/rest/issues/" + issueId + "/files/" + fileId;
            method = Method.GET;
            headers.Add("Authorization", JsonBuilder.ReturnParameterAppSettings("TOKEN"));
        }
    }
}
