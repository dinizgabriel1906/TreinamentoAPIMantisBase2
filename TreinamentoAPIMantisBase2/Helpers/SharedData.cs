using System;
using System.Collections.Generic;
using System.Text;

namespace TreinamentoAPIMantisBase2.Helpers

{
    public class SharedData
    {
        public string petId;
        public string petName;
        public string petCategoryId;
        public string petCategoryName;
        public string petphotoURL;
        public string petTagId;
        public string petTagName;
        public string petStatus;

        public string orderId;
        public string orderQuantity;
        public string orderShipDate;
        public string orderStatus;
        public string orderComplete;
    }
}
