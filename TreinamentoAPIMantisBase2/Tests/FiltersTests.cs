using NUnit.Framework;
using RestSharp;
using TreinamentoAPIMantisBase2.Bases;
using TreinamentoAPIMantisBase2.Objects;
using TreinamentoAPIMantisBase2.Requests.Issues;
using System;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreinamentoAPIMantisBase2.Tests 
{
    [TestFixture]
    public class FiltersTests : TestBase 
    {

        private string filterId;

        public FiltersTests()
        {
            filterId = "1";
            //inicializar user
        }

        #region Testing status code OK
        [Test]
        public void GetAllFiltersStatusCodeOKTest() {
            string statusCodeEsperado = "OK";

            GetAllFiltersRequest getAllFiltersRequest = new GetAllFiltersRequest();
            IRestResponse<dynamic> response = getAllFiltersRequest.ExecuteRequest();

            Assert.AreEqual(statusCodeEsperado, response.StatusCode.ToString());
        }

        [Test]
        public void GetFilterByIdStatusCodeOKTest() {
            string statusCodeEsperado = "OK";

            GetFilterByIdRequest getFilterByIdRequest = new GetFilterByIdRequest(filterId);
            IRestResponse<dynamic> response = getFilterByIdRequest.ExecuteRequest();

            Assert.AreEqual(statusCodeEsperado, response.StatusCode.ToString());
        }

        [Test]
        public void DellFilterStatusCodeOKTest() {
            string statusCodeEsperado = "OK";

            DelFilterRequest delFilterRequest = new DelFilterRequest(filterId);
            IRestResponse<dynamic> response = delFilterRequest.ExecuteRequest();

            Assert.AreEqual(statusCodeEsperado, response.StatusCode.ToString());
        }
        #endregion
    }
}