using NUnit.Framework;
using RestSharp;
using TreinamentoAPIMantisBase2.Bases;
using TreinamentoAPIMantisBase2.Objects;
using TreinamentoAPIMantisBase2.Requests.Issues;
using System;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreinamentoAPIMantisBase2.Tests 
{

    [TestFixture]
    public class UsersTests : TestBase 
    {
        private User user;
        private string userId;

        public UsersTests()
        {
            userId = "1";
            //inicializar user
        }

        #region Testing status code OK
        [Test]
        public void GetUserInfoStatusCodeOKTest() {
            string statusCodeEsperado = "OK";

            GetUserInfoRequest getUserInfoRequest = new GetUserInfoRequest();
            IRestResponse<dynamic> response = getUserInfoRequest.ExecuteRequest();

            Assert.AreEqual(statusCodeEsperado, response.StatusCode.ToString());
        }

        [Test]
        public void GetIssueByIdStatusCodeOKTest() {
            string statusCodeEsperado = "OK";

            GetIssueByIdRequest getIssueByIdRequest = new GetIssueByIdRequest("1");
            IRestResponse<dynamic> response = getIssueByIdRequest.ExecuteRequest();

            Assert.AreEqual(statusCodeEsperado, response.StatusCode.ToString());
        }

        [Test]
        public void CreateUserMinimalStatusCodeOKTest() {
            string statusCodeEsperado = "OK";

            PostCreateUserMinimalRequest postCreateUserMinimalRequest = new PostCreateUserMinimalRequest();
            postCreateUserMinimalRequest.setJsonBody(user);
            IRestResponse<dynamic> response = postCreateUserMinimalRequest.ExecuteRequest();

            Assert.AreEqual(statusCodeEsperado, response.StatusCode.ToString());
        }

        [Test]
        public void CreateUserStatusCodeOKTest() {
            string statusCodeEsperado = "OK";

            PostCreateUserRequest postCreateUserRequest = new PostCreateUserRequest();
            postCreateUserRequest.setJsonBody(user);
            IRestResponse<dynamic> response = postCreateUserRequest.ExecuteRequest();

            Assert.AreEqual(statusCodeEsperado, response.StatusCode.ToString());
        }

        [Test]
        public void ResetUserPasswordStatusCodeOKTest() {
            string statusCodeEsperado = "OK";

            PutResetUserPasswordRequest putResetUserPasswordRequest = new PutResetUserPasswordRequest(userId);
            IRestResponse<dynamic> response = putResetUserPasswordRequest.ExecuteRequest();

            Assert.AreEqual(statusCodeEsperado, response.StatusCode.ToString());
        }

        [Test]
        public void DelUserStatusCodeOKTest() {
            string statusCodeEsperado = "OK";

            DelUserRequest delUserRequest = new DelUserRequest(userId);
            IRestResponse<dynamic> response = delUserRequest.ExecuteRequest();

            Assert.AreEqual(statusCodeEsperado, response.StatusCode.ToString());
        }
        #endregion
    }
}