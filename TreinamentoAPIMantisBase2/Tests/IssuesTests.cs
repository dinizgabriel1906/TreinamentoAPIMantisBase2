using NUnit.Framework;
using RestSharp;
using TreinamentoAPIMantisBase2.Bases;
using TreinamentoAPIMantisBase2.Objects;
using TreinamentoAPIMantisBase2.Requests.Issues;
using System;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreinamentoAPIMantisBase2.Tests 
{

    [TestFixture]
    public class IssuesTests : TestBase 
    {
        private Issue issue;
        private string pageSize;
        private string page;

        public IssuesTests()
        {
            pageSize = "10";
            page = "1";
            //inicializar issue
        }

        #region Testing Status Code OK
        [Test]
        public void GetIssueByIdStatusCodeOKTest() {
            string statusCodeEsperado = "OK";

            GetIssueByIdRequest getIssueByIdRequest = new GetIssueByIdRequest("1");
            IRestResponse<dynamic> response = getIssueByIdRequest.ExecuteRequest();

            Assert.AreEqual(statusCodeEsperado, response.StatusCode.ToString());
        }

        [Test]
        public void GetAllIssuesStatusCodeOKTest() {
            string statusCodeEsperado = "OK";

            GetAllIssuesRequest getAllIssuesRequest = new GetAllIssuesRequest(pageSize, page);
            IRestResponse<dynamic> response = getAllIssuesRequest.ExecuteRequest();

            Assert.AreEqual(statusCodeEsperado, response.StatusCode.ToString());
        }

        [Test]
        public void DelIssueStatusCodeOKTest() {
            string statusCodeEsperado = "OK";

            DelIssueRequest delIssueRequest = new DelIssueRequest("1");
            IRestResponse<dynamic> response = delIssueRequest.ExecuteRequest();

            Assert.AreEqual(statusCodeEsperado, response.StatusCode.ToString());
        }
        #endregion
    }
}